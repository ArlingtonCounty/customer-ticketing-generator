﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Customer_Ticket_Generator.Models
{
    public class Ticket
    {
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone no.")]
        [Display(Name = "Phone Number")]
        [Required]
        public string PhoneNumber { get; set; }
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [Required]
        public string Email { get; set; }
        [Display(Name = "Description")]
        [Required]
        public string Description { get; set; }
        [Display(Name = "Permit Number/Project ID")]
        public string PermitNumber { get; set; }

       // public RECaptcha Captcha { get; set; }
       // public string hfCaptcha { get; set; }
    }

    public class JsonTicket
    {

        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string description { get; set; }
        public string permit_id { get; set; }
    }


    public class Result
    {
        public string number { get; set; }
    }

    public class ResultBody
    {
        public Result result { get; set; }
    }

}