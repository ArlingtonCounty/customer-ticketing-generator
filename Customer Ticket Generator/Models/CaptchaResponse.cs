﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Customer_Ticket_Generator.Models
{
    public class CaptchaResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("error-codes")]
        public List<string> ErrorMessage { get; set; }
    }

    public class RECaptcha
    {
        public string Key = System.Web.Configuration.WebConfigurationManager.AppSettings["recaptchaPublickey"];

        public string Secret = System.Web.Configuration.WebConfigurationManager.AppSettings["recaptchaPrivatekey"];
        public string Response { get; set; }

    }
}