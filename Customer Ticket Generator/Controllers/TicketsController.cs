﻿using Customer_Ticket_Generator.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
//using System.Web.Script.Serialization;

namespace Customer_Ticket_Generator.Controllers
{
    public class TicketsController : Controller
    {
        // GET: Tickets
        public ActionResult Index()
        {


            return RedirectToAction("Create");
        }



        // GET: Tickets/Create
        public ActionResult Create()
        {
            ViewBag.PublicKey = System.Web.Configuration.WebConfigurationManager.AppSettings["recaptchaPublickey"];
            return View();
        }

        public static CaptchaResponse ValidateCaptcha(string response)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            string secret = System.Web.Configuration.WebConfigurationManager.AppSettings["recaptchaPrivateKey"];
            var client = new WebClient();
            var jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
            return JsonConvert.DeserializeObject<CaptchaResponse>(jsonResult.ToString());
        }

        //public class CaptchaResponse
        //{
        //    [JsonProperty("success")]
        //    public bool Success { get; set; }
        //    [JsonProperty("error-codes")]
        //    public List<string> ErrorMessage { get; set; }
        //}

        // POST: Tickets/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ticket t)
        {

            try
            {

                CaptchaResponse response = ValidateCaptcha(Request["g-recaptcha-response"]);
                //var test = new Models.CaptchaResponse();

                if (ModelState.IsValid && response.Success) {
                    if (!Helpers.CWHelper.isValid(t))
                    {
                        Helpers.EmailHelper.sendErrorEmail(null, "Ticket blocked", t);
                        return RedirectToAction("Error");
                    }
                    // Helpers.EmailHelper.SendTicketEmail(t);

                    var number = Helpers.CWHelper.CallWebService(t);
                    return RedirectToAction("Success", new { n = number });
                }
                else {
                    return RedirectToAction("Error");
                }

            }catch(Exception ex)
            {

                Helpers.EmailHelper.sendErrorEmail(ex,"Error After ticket created", t);
                return RedirectToAction("Error");
            }
            
            return RedirectToAction("Error");
        }


        public ActionResult Success(string n)
        {
            @ViewBag.number = n;
            return View();
        }


        public ActionResult Error()
        {
            return View();
        }
    }
}
