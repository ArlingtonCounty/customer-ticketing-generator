﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customer_Ticket_Generator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Create", "Tickets");
            return View();
        }

        public ActionResult About()
        {
            return RedirectToAction("Create", "Tickets");
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return RedirectToAction("Create", "Tickets");
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}