﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Customer_Ticket_Generator.Models;


namespace Customer_Ticket_Generator.Helpers
{
    public class EmailHelper
    {


        public static void SendTicketEmail(Ticket t)
        {
            string from = ConfigurationManager.AppSettings["FromEmail"];
            string recpient = ConfigurationManager.AppSettings["ToEmail"];


            string messageText = "";
            //t.FirstName, t.LastName, t.PhoneNumber, t.Email, t.Description);
            messageText += "Ticket Created by: " + t.FirstName + " " + t.LastName +"\n";
            messageText += "Created on: " + DateTime.Now.ToString() + "\n";
            messageText += "Phone Number: " + t.PhoneNumber + "\n";
            messageText += "Email: " + t.Email + "\n"; 
            messageText += "Permit Number:" + t.PermitNumber + "\n"; 

            messageText += "Description: " + t.Description + "\n";

            System.Net.Mail.MailMessage emessage = new System.Net.Mail.MailMessage(from, recpient, "Ticket Generator Ticket", messageText);
            emessage.IsBodyHtml = false;

            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("smtpout.arlingtonva.us");
            client.Send(emessage);
            client.Dispose();
        }
            /// <summary>
            /// This is a class for diagnosing the problem with CPHD crashes 
            ///
            /// </summary>
            /// <param name="context"></param>
            public static void sendErrorEmail(Exception e, string message = "", Ticket t = null)
        {
            string from = ConfigurationManager.AppSettings["ToEmailError"];
            string recpient = ConfigurationManager.AppSettings["FromEmailError"];
            string messageText = "" + DateTime.Now.ToString() + " " + message + '\n';

            if (e != null)
            {
                if (e.Message != null)
                {
                    messageText += "Exception error message" + e.Message + '\n';
                }

                if (e.InnerException != null)
                {
                    messageText += "Exception InnerException" + e.InnerException + '\n';
                }
            }

            if (t != null)
            {
                if (t.FirstName != null)
                {
                    messageText += "First Name: " + t.FirstName + '\n';
                }
                else
                {
                    messageText += "First Name Null" +'\n';
                }


                if (t.LastName != null)
                {
                    messageText += "Last Name: " + t.LastName + '\n';
                }
                else
                {
                    messageText += "Last Name Null" + '\n';
                }

                if (t.PermitNumber != null)
                {
                    messageText += "PermitNumber: " + t.PermitNumber + '\n';
                }
                else
                {
                    messageText += "PermitNumber Null" + '\n';
                }


                if (t.PhoneNumber != null)
                {
                    messageText += "PhoneNumber: " + t.PhoneNumber + '\n';
                }
                else
                {
                    messageText += "PhoneNumber Null" + '\n';
                }


                if (t.Description != null)
                {
                    messageText += "Description: " + t.Description + '\n';
                }
                else
                {
                    messageText += "Description Null" + '\n';
                }


                if (t.Email != null)
                {
                    messageText += "Email: " + t.Email + '\n';
                }
                else
                {
                    messageText += "Email Null" + '\n';
                }
            }
            else
            {
                messageText += "No ticket data passed in. "  + '\n';
            }

            if (messageText == "")
            {
                messageText = "Error with null message";
            }

            System.Net.Mail.MailMessage emessage = new System.Net.Mail.MailMessage(from, recpient, "Error when Ticket Generator", messageText);
            emessage.IsBodyHtml = false;

            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("smtpout.arlingtonva.us");
            client.Send(emessage);
            client.Dispose();


        }
    }
}