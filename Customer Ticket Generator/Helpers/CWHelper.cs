﻿using System;
using System.Net;
using System.IO;
using Customer_Ticket_Generator.Models;
using System.Text;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Customer_Ticket_Generator.Helpers
{
    public class CWHelper
    {


        public static bool isValid(Ticket t)
        {

            if (t == null)
            {
                throw new ArgumentNullException("ticket");
            }

           var invalidStrings = ConfigurationManager.AppSettings["inValidValues"].Split(',');

            foreach (var invalidString in invalidStrings)
            {
                var lowerinvalidString = invalidString.ToLower();

                if (t.FirstName.ToLower().Contains(lowerinvalidString))
                {
                    return false;
                }
                if (t.LastName.ToLower().Contains(lowerinvalidString))
                {
                    return false;
                }
                if (t.PhoneNumber.ToLower().Contains(lowerinvalidString))
                {
                    return false;
                }
                if (t.Email.ToLower().Contains(lowerinvalidString))
                {
                    return false;
                }
                if (t.Description.ToLower().Contains(lowerinvalidString))
                {
                    return false;
                }
                if ((t.PermitNumber != null) &&(t.PermitNumber.ToLower().Contains(lowerinvalidString)))
                {
                    return false;
                }
            }

            return true;
        }
        public static string CallWebService(Ticket t)
        {
           //return "1111111111";
           return  CallWebService(t.FirstName, t.LastName, t.PhoneNumber, t.Email, t.Description, t.PermitNumber);
        }

        public static string CallWebService(string firstname, string lastname, string phone, string email, string description, string permit_id)
        {
            JsonTicket t = new JsonTicket();
            // var json = "";
            var info = "";

            if (permit_id != null)
            {
                info = "Project ID: " + permit_id + "\n";
                info = Regex.Replace(info, "[^a-zA-Z0-9\x20\x40\x40\x2E\x2D]", " ");
            }

                t.first_name = firstname;
            t.last_name = lastname;
            t.email = email;
            t.phone = phone;

            info += "Phone:" + phone + "\nCaller: " + firstname + " " + lastname+  "\nEmail: " + email;
            //Regex rgx = new Regex("[^a-zA-Z0-9\x20\x40\x40\x2E]");
            t.description = Regex.Replace(description, "[^a-zA-Z0-9\x20\x40\x40\x2E\x2D]", " ");
           // t.description = t.description.Replace(@"'", " ").Replace(@"%", " ");
            t.description = t.description + '\n' + info;


            t.permit_id = permit_id;
            string json = new JavaScriptSerializer().Serialize(t);
            //staging
            //string URL = @"https://egov.arlingtonva.us/stg-AcgCWProxy/api/PermitArlington";
            string URL = ConfigurationManager.AppSettings["APIURL"];
              //  @"https://egov.arlingtonva.us/AcgPAProxy/api/PermitArlington";




            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(ConfigurationManager.AppSettings["APIAUTH"]));
            //request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("acgproxy:Arl!ngt0n"));

            request.ContentLength = json.Length;
            using (Stream webStream = request.GetRequestStream())
            using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
            {
                requestWriter.Write(json);
            }

            //try
            //{
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();

                var data = new JavaScriptSerializer().Deserialize<ResultBody>(response);


                return data.result.number;
                    //  Console.Out.WriteLine(response);
                }

            //}
            //catch (Exception e)
            //{
            //    Console.Out.WriteLine("-----------------");
            //    Console.Out.WriteLine(e.Message);
            //    return "Error" + e.Message;
            //}



        }

        //        public static string CallWebService(string firstname, string lastname, string phone, string email, string description)
        //        {



        //            var _url = "https://egov.arlingtonva.us/AcgPAProxy/proxy/ACGBPProxyAPI.asmx";
        //            //var _action = "https://egov.arlingtonva.us/AcgPAProxy/proxy/ACGBPProxyAPI.asmx?op=CreateCWPermitIncident";
        //            var _action = "https://egov.arlingtonva.us/AcgCWProxy/proxy/CreateCWPermitIncident";

        //            XmlDocument soapEnvelopeXml = CreateSoapEnvelope(firstname, lastname, phone, email, description);
        //            HttpWebRequest webRequest = CreateWebRequest(_url, _action);
        //            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

        //            Debug.WriteLine(soapEnvelopeXml.ToString());

        //            // begin async call to web request.
        //            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

        //            // suspend this thread until call is complete. You might want to
        //            // do something usefull here like update your UI.
        //            asyncResult.AsyncWaitHandle.WaitOne();

        //            // get the response from the completed web request.
        //            string soapResult;
        //            try
        //            {
        //                using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
        //                {
        //                    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
        //                    {
        //                        soapResult = rd.ReadToEnd();
        //                    }
        //                    return GetCWNumber(soapResult);

        //                }
        //            }
        //            catch (Exception ex)
        //            {

        //            }

        //            return null;
        //        }

        //        private static HttpWebRequest CreateWebRequest(string url, string action)
        //        {
        //            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
        //            webRequest.Headers.Add("SOAPAction", action);
        //            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        //            webRequest.Accept = "text/xml";
        //            webRequest.Method = "POST";
        //            //webRequest.Credentials = new System.Net.NetworkCredential("ACG_ProxyAPI", "Arl!ngtonApI");
        //            //webRequest.Method = "CreateCWPermitIncident";
        //            return webRequest;
        //        }

        //        private static XmlDocument CreateSoapEnvelope(string firstname, string lastname, string phone, string email, string description)
        //        {
        //            XmlDocument soapEnvelopeDocument = new XmlDocument();
        //            soapEnvelopeDocument.LoadXml(@"<soap:Envelope xmlns:xsi = ""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd = ""http://www.w3.org/2001/XMLSchema"" xmlns:soap = ""http://schemas.xmlsoap.org/soap/envelope/"">
        //  <soap:Header>
        //    <ProxyHeader xmlns = ""https://egov.arlingtonva.us/AcgCWProxy/proxy/"">
        //      <Username>" + ConfigurationManager.AppSettings["APILogin"] + @"</Username>
        //      <Password>" + ConfigurationManager.AppSettings["APIPassword"] + @"</Password>
        //    </ProxyHeader>
        //  </soap:Header>
        //  <soap:Body>
        //    <CreateCWPermitIncident xmlns = ""https://egov.arlingtonva.us/AcgCWProxy/proxy/"">
        //      <firstName >" + firstname + @"</firstName>
        //      <lasttName >" + lastname + @"</lasttName>
        //      <phone >" + phone + @"</phone >
        //      <emalAddress >" + email + @"</emalAddress>
        //      <description >" + description + @"</description>
        //    </CreateCWPermitIncident>
        //  </soap:Body>
        //</soap:Envelope>
        //");



        //            //            soapEnvelopeDocument.LoadXml(@" < SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema"">
        //            //<SOAP-ENV:Body>
        //            //<CreateCWPermitIncident  xmlns=""https://egov.arlingtonva.us/AcgCWProxy/proxy/"">
        //            //      <firstName>Ian</firstName>
        //            //      <lasttName>Maloney</lasttName>
        //            //      <phone>7032283582</phone>
        //            //      <emalAddress>Test@arlingtonva.us</emalAddress>
        //            //<description>This is a test</description>
        //            //</CreateCWPermitIncident>
        //            //</SOAP-ENV:Body></SOAP-ENV:Envelope>");
        //            return soapEnvelopeDocument;
        //        }

        //        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        //        {
        //            using (Stream stream = webRequest.GetRequestStream())
        //            {
        //                soapEnvelopeXml.Save(stream);
        //            }
        //        }

        //        private static string GetCWNumber(string xml)
        //        {
        //            XmlDocument responce = new XmlDocument();
        //            responce.LoadXml(xml);

        //            //web-s4 and web-s5
        //            try
        //            {
        //                //foreach(XmlNodeList x in responce.ChildNodes)
        //                //{
        //                //    Debug.WriteLine(x);
        //                //}
        //                return responce["soap:Envelope"]["soap:Body"]["CreateCWPermitIncidentResponse"].InnerText;
        //                //var node = responce.SelectSingleNode("soap:Envelope/soap:Body/CreateCWPermitIncidentResponse/CreateCWPermitIncidentResponse");
        //                //return node.InnerText;

        //            }
        //            catch (Exception ex)
        //            {

        //            }
        //            responce.ToString();

        //            return null;
        //        }

            }

    }