﻿using System.Web;
using System.Web.Mvc;

namespace Customer_Ticket_Generator
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
