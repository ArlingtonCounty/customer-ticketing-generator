﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Customer_Ticket_Generator.Startup))]
namespace Customer_Ticket_Generator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
